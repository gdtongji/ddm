package org.snlab.compiler;

import java.util.List;

import org.jgrapht.GraphPath;
import org.jgrapht.alg.interfaces.KShortestPathAlgorithm;
import org.jgrapht.alg.shortestpath.EppsteinKShortestPath;
import org.jgrapht.alg.shortestpath.FloydWarshallShortestPaths;
import org.jgrapht.alg.shortestpath.YenKShortestPath;

public class DFAGenerator {
    private Topology topology;

    public DFAGenerator(Topology topology) {
        this.topology = topology;
    }

    public void simple() {

    }

    public void shortest() {
        
    }

    public void ecmp(String from, String to) {
        YenKShortestPath eksp = new YenKShortestPath<>(topology.getGraph());
        List<GraphPath> paths = eksp.getPaths(topology.getDeviceByName(from), topology.getDeviceByName(to), 1000);
        System.out.println(paths.size());
        // FloydWarshallShortestPaths aPaths = new FloydWarshallShortestPaths<>(this.topology.getGraph());
        // System.out.println(aPaths.getShortestPathsCount());
    }

    public static void main(String[] args) {
        Topology topology = Topology.fromFile("envs/fb/topo.txt");
        DFAGenerator generator = new DFAGenerator(topology);
        generator.ecmp("rsw-0-0", "fsw-0-0");
    }
}
