package org.snlab.compiler;

import java.util.List;


public class DownstreamConf {
    private long vid;
    private String device;
    private String addr;
    private List<String> via;

    public DownstreamConf(long vid, String device, String addr) {
        this.vid = vid;
        this.device = device;
        this.addr = addr;
    }

    /**
     * @return long return the vid
     */
    public long getVid() {
        return vid;
    }

    /**
     * @param vid the vid to set
     */
    public void setVid(long vid) {
        this.vid = vid;
    }

    /**
     * @return String return the device
     */
    public String getDevice() {
        return device;
    }

    /**
     * @param device the device to set
     */
    public void setDevice(String device) {
        this.device = device;
    }

    /**
     * @return List<String> return the via
     */
    public List<String> getVia() {
        return via;
    }

    /**
     * @param via the via to set
     */
    public void setVia(List<String> via) {
        this.via = via;
    }


    /**
     * @return String return the addr
     */
    public String getAddr() {
        return addr;
    }

    /**
     * @param addr the addr to set
     */
    public void setAddr(String addr) {
        this.addr = addr;
    }

}
