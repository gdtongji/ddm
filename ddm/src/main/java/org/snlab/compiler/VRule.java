package org.snlab.compiler;

import java.util.HashSet;
import java.util.Set;

public class VRule {
    private long match;
    private Set<Nexthop> nexthops = new HashSet<>();

    /**
     * @return long return the match
     */
    public long getMatch() {
        return match;
    }

    /**
     * @param match the match to set
     */
    public void setMatch(long match) {
        this.match = match;
    }

    /**
     * @return Set<Nexthop> return the nexthops
     */
    public Set<Nexthop> getNexthops() {
        return nexthops;
    }

    /**
     * @param nexthops the nexthops to set
     */
    public void setNexthops(Set<Nexthop> nexthops) {
        this.nexthops = nexthops;
    }

}
