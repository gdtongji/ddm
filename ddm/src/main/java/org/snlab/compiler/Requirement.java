package org.snlab.compiler;

public class Requirement {
    private int hs;
    private DFA dfa;

    /**
     * @return int return the hs
     */
    public int getHs() {
        return hs;
    }

    /**
     * @param hs the hs to set
     */
    public void setHs(int hs) {
        this.hs = hs;
    }

    /**
     * @return DFA return the dfa
     */
    public DFA getDfa() {
        return dfa;
    }

    /**
     * @param dfa the dfa to set
     */
    public void setDfa(DFA dfa) {
        this.dfa = dfa;
    }

}
