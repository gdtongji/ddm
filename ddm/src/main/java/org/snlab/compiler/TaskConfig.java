package org.snlab.compiler;

import java.util.List;

public class TaskConfig {
    private int id;
    private DFA dfa;
    private List<String> Vnodes; // the set of nodes of DFA on the device
    private List<String> downstreams;
}
