package org.snlab.compiler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

public class DDMConf {
    private List<VnodeConf> vnodeConfs = new ArrayList<>();

    public void dump(String out_file) {
        ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
        try {
            objectMapper.writeValue(new File(out_file), this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return List<VnodeConf> return the vnodeConfs
     */
    public List<VnodeConf> getVnodeConfs() {
        return vnodeConfs;
    }

    /**
     * @param vnodeConfs the vnodeConfs to set
     */
    public void setVnodeConfs(List<VnodeConf> vnodeConfs) {
        this.vnodeConfs = vnodeConfs;
    }

}
