package org.snlab.compiler;

import java.util.ArrayList;
import java.util.List;

public class VnodeConf {
    private long vid;
    private String device;
    private List<DownstreamConf> downstreamConfs = new ArrayList<>();

    public VnodeConf(long vid, String device) {
        this.vid = vid;
        this.device = device;
    }

    /**
     * @return long return the vid
     */
    public long getVid() {
        return vid;
    }

    /**
     * @param vid the vid to set
     */
    public void setVid(long vid) {
        this.vid = vid;
    }

    /**
     * @return String return the device
     */
    public String getDevice() {
        return device;
    }

    /**
     * @param device the device to set
     */
    public void setDevice(String device) {
        this.device = device;
    }

    /**
     * @return List<DownstreamConf> return the downstreamConfs
     */
    public List<DownstreamConf> getDownstreamConfs() {
        return downstreamConfs;
    }

    /**
     * @param downstreamConfs the downstreamConfs to set
     */
    public void setDownstreamConfs(List<DownstreamConf> downstreamConfs) {
        this.downstreamConfs = downstreamConfs;
    }

}
