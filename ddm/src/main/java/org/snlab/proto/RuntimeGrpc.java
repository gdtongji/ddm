package org.snlab.proto;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.9.1)",
    comments = "Source: ddmpt.proto")
public final class RuntimeGrpc {

  private RuntimeGrpc() {}

  public static final String SERVICE_NAME = "org.snlab.proto.Runtime";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getProcessMethod()} instead. 
  public static final io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Msg,
      org.snlab.proto.Ddmpt.Empty> METHOD_PROCESS = getProcessMethod();

  private static volatile io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Msg,
      org.snlab.proto.Ddmpt.Empty> getProcessMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Msg,
      org.snlab.proto.Ddmpt.Empty> getProcessMethod() {
    io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Msg, org.snlab.proto.Ddmpt.Empty> getProcessMethod;
    if ((getProcessMethod = RuntimeGrpc.getProcessMethod) == null) {
      synchronized (RuntimeGrpc.class) {
        if ((getProcessMethod = RuntimeGrpc.getProcessMethod) == null) {
          RuntimeGrpc.getProcessMethod = getProcessMethod = 
              io.grpc.MethodDescriptor.<org.snlab.proto.Ddmpt.Msg, org.snlab.proto.Ddmpt.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "org.snlab.proto.Runtime", "process"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.snlab.proto.Ddmpt.Msg.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.snlab.proto.Ddmpt.Empty.getDefaultInstance()))
                  .setSchemaDescriptor(new RuntimeMethodDescriptorSupplier("process"))
                  .build();
          }
        }
     }
     return getProcessMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getSetTaskMethod()} instead. 
  public static final io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Task,
      org.snlab.proto.Ddmpt.Empty> METHOD_SET_TASK = getSetTaskMethod();

  private static volatile io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Task,
      org.snlab.proto.Ddmpt.Empty> getSetTaskMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Task,
      org.snlab.proto.Ddmpt.Empty> getSetTaskMethod() {
    io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Task, org.snlab.proto.Ddmpt.Empty> getSetTaskMethod;
    if ((getSetTaskMethod = RuntimeGrpc.getSetTaskMethod) == null) {
      synchronized (RuntimeGrpc.class) {
        if ((getSetTaskMethod = RuntimeGrpc.getSetTaskMethod) == null) {
          RuntimeGrpc.getSetTaskMethod = getSetTaskMethod = 
              io.grpc.MethodDescriptor.<org.snlab.proto.Ddmpt.Task, org.snlab.proto.Ddmpt.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "org.snlab.proto.Runtime", "setTask"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.snlab.proto.Ddmpt.Task.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.snlab.proto.Ddmpt.Empty.getDefaultInstance()))
                  .setSchemaDescriptor(new RuntimeMethodDescriptorSupplier("setTask"))
                  .build();
          }
        }
     }
     return getSetTaskMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getAddVnodeMethod()} instead. 
  public static final io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Vnode,
      org.snlab.proto.Ddmpt.Empty> METHOD_ADD_VNODE = getAddVnodeMethod();

  private static volatile io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Vnode,
      org.snlab.proto.Ddmpt.Empty> getAddVnodeMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Vnode,
      org.snlab.proto.Ddmpt.Empty> getAddVnodeMethod() {
    io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Vnode, org.snlab.proto.Ddmpt.Empty> getAddVnodeMethod;
    if ((getAddVnodeMethod = RuntimeGrpc.getAddVnodeMethod) == null) {
      synchronized (RuntimeGrpc.class) {
        if ((getAddVnodeMethod = RuntimeGrpc.getAddVnodeMethod) == null) {
          RuntimeGrpc.getAddVnodeMethod = getAddVnodeMethod = 
              io.grpc.MethodDescriptor.<org.snlab.proto.Ddmpt.Vnode, org.snlab.proto.Ddmpt.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "org.snlab.proto.Runtime", "addVnode"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.snlab.proto.Ddmpt.Vnode.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.snlab.proto.Ddmpt.Empty.getDefaultInstance()))
                  .setSchemaDescriptor(new RuntimeMethodDescriptorSupplier("addVnode"))
                  .build();
          }
        }
     }
     return getAddVnodeMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getAddVnodesMethod()} instead. 
  public static final io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Vnodes,
      org.snlab.proto.Ddmpt.Empty> METHOD_ADD_VNODES = getAddVnodesMethod();

  private static volatile io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Vnodes,
      org.snlab.proto.Ddmpt.Empty> getAddVnodesMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Vnodes,
      org.snlab.proto.Ddmpt.Empty> getAddVnodesMethod() {
    io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Vnodes, org.snlab.proto.Ddmpt.Empty> getAddVnodesMethod;
    if ((getAddVnodesMethod = RuntimeGrpc.getAddVnodesMethod) == null) {
      synchronized (RuntimeGrpc.class) {
        if ((getAddVnodesMethod = RuntimeGrpc.getAddVnodesMethod) == null) {
          RuntimeGrpc.getAddVnodesMethod = getAddVnodesMethod = 
              io.grpc.MethodDescriptor.<org.snlab.proto.Ddmpt.Vnodes, org.snlab.proto.Ddmpt.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "org.snlab.proto.Runtime", "addVnodes"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.snlab.proto.Ddmpt.Vnodes.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.snlab.proto.Ddmpt.Empty.getDefaultInstance()))
                  .setSchemaDescriptor(new RuntimeMethodDescriptorSupplier("addVnodes"))
                  .build();
          }
        }
     }
     return getAddVnodesMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getInsertEntryMethod()} instead. 
  public static final io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Entry,
      org.snlab.proto.Ddmpt.Empty> METHOD_INSERT_ENTRY = getInsertEntryMethod();

  private static volatile io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Entry,
      org.snlab.proto.Ddmpt.Empty> getInsertEntryMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Entry,
      org.snlab.proto.Ddmpt.Empty> getInsertEntryMethod() {
    io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Entry, org.snlab.proto.Ddmpt.Empty> getInsertEntryMethod;
    if ((getInsertEntryMethod = RuntimeGrpc.getInsertEntryMethod) == null) {
      synchronized (RuntimeGrpc.class) {
        if ((getInsertEntryMethod = RuntimeGrpc.getInsertEntryMethod) == null) {
          RuntimeGrpc.getInsertEntryMethod = getInsertEntryMethod = 
              io.grpc.MethodDescriptor.<org.snlab.proto.Ddmpt.Entry, org.snlab.proto.Ddmpt.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "org.snlab.proto.Runtime", "insertEntry"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.snlab.proto.Ddmpt.Entry.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.snlab.proto.Ddmpt.Empty.getDefaultInstance()))
                  .setSchemaDescriptor(new RuntimeMethodDescriptorSupplier("insertEntry"))
                  .build();
          }
        }
     }
     return getInsertEntryMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getShowVroutesMethod()} instead. 
  public static final io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Empty,
      org.snlab.proto.Ddmpt.Vroutes> METHOD_SHOW_VROUTES = getShowVroutesMethod();

  private static volatile io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Empty,
      org.snlab.proto.Ddmpt.Vroutes> getShowVroutesMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Empty,
      org.snlab.proto.Ddmpt.Vroutes> getShowVroutesMethod() {
    io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Empty, org.snlab.proto.Ddmpt.Vroutes> getShowVroutesMethod;
    if ((getShowVroutesMethod = RuntimeGrpc.getShowVroutesMethod) == null) {
      synchronized (RuntimeGrpc.class) {
        if ((getShowVroutesMethod = RuntimeGrpc.getShowVroutesMethod) == null) {
          RuntimeGrpc.getShowVroutesMethod = getShowVroutesMethod = 
              io.grpc.MethodDescriptor.<org.snlab.proto.Ddmpt.Empty, org.snlab.proto.Ddmpt.Vroutes>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "org.snlab.proto.Runtime", "showVroutes"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.snlab.proto.Ddmpt.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.snlab.proto.Ddmpt.Vroutes.getDefaultInstance()))
                  .setSchemaDescriptor(new RuntimeMethodDescriptorSupplier("showVroutes"))
                  .build();
          }
        }
     }
     return getShowVroutesMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getVerifyMethod()} instead. 
  public static final io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.VerifyTask,
      org.snlab.proto.Ddmpt.Empty> METHOD_VERIFY = getVerifyMethod();

  private static volatile io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.VerifyTask,
      org.snlab.proto.Ddmpt.Empty> getVerifyMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.VerifyTask,
      org.snlab.proto.Ddmpt.Empty> getVerifyMethod() {
    io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.VerifyTask, org.snlab.proto.Ddmpt.Empty> getVerifyMethod;
    if ((getVerifyMethod = RuntimeGrpc.getVerifyMethod) == null) {
      synchronized (RuntimeGrpc.class) {
        if ((getVerifyMethod = RuntimeGrpc.getVerifyMethod) == null) {
          RuntimeGrpc.getVerifyMethod = getVerifyMethod = 
              io.grpc.MethodDescriptor.<org.snlab.proto.Ddmpt.VerifyTask, org.snlab.proto.Ddmpt.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "org.snlab.proto.Runtime", "verify"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.snlab.proto.Ddmpt.VerifyTask.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.snlab.proto.Ddmpt.Empty.getDefaultInstance()))
                  .setSchemaDescriptor(new RuntimeMethodDescriptorSupplier("verify"))
                  .build();
          }
        }
     }
     return getVerifyMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getShowVnodesMethod()} instead. 
  public static final io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Empty,
      org.snlab.proto.Ddmpt.Vnodes> METHOD_SHOW_VNODES = getShowVnodesMethod();

  private static volatile io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Empty,
      org.snlab.proto.Ddmpt.Vnodes> getShowVnodesMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Empty,
      org.snlab.proto.Ddmpt.Vnodes> getShowVnodesMethod() {
    io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Empty, org.snlab.proto.Ddmpt.Vnodes> getShowVnodesMethod;
    if ((getShowVnodesMethod = RuntimeGrpc.getShowVnodesMethod) == null) {
      synchronized (RuntimeGrpc.class) {
        if ((getShowVnodesMethod = RuntimeGrpc.getShowVnodesMethod) == null) {
          RuntimeGrpc.getShowVnodesMethod = getShowVnodesMethod = 
              io.grpc.MethodDescriptor.<org.snlab.proto.Ddmpt.Empty, org.snlab.proto.Ddmpt.Vnodes>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "org.snlab.proto.Runtime", "showVnodes"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.snlab.proto.Ddmpt.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.snlab.proto.Ddmpt.Vnodes.getDefaultInstance()))
                  .setSchemaDescriptor(new RuntimeMethodDescriptorSupplier("showVnodes"))
                  .build();
          }
        }
     }
     return getShowVnodesMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getDeleteRuleMethod()} instead. 
  public static final io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Empty,
      org.snlab.proto.Ddmpt.Empty> METHOD_DELETE_RULE = getDeleteRuleMethod();

  private static volatile io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Empty,
      org.snlab.proto.Ddmpt.Empty> getDeleteRuleMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Empty,
      org.snlab.proto.Ddmpt.Empty> getDeleteRuleMethod() {
    io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Empty, org.snlab.proto.Ddmpt.Empty> getDeleteRuleMethod;
    if ((getDeleteRuleMethod = RuntimeGrpc.getDeleteRuleMethod) == null) {
      synchronized (RuntimeGrpc.class) {
        if ((getDeleteRuleMethod = RuntimeGrpc.getDeleteRuleMethod) == null) {
          RuntimeGrpc.getDeleteRuleMethod = getDeleteRuleMethod = 
              io.grpc.MethodDescriptor.<org.snlab.proto.Ddmpt.Empty, org.snlab.proto.Ddmpt.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "org.snlab.proto.Runtime", "deleteRule"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.snlab.proto.Ddmpt.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.snlab.proto.Ddmpt.Empty.getDefaultInstance()))
                  .setSchemaDescriptor(new RuntimeMethodDescriptorSupplier("deleteRule"))
                  .build();
          }
        }
     }
     return getDeleteRuleMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getDumpMethod()} instead. 
  public static final io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Empty,
      org.snlab.proto.Ddmpt.Empty> METHOD_DUMP = getDumpMethod();

  private static volatile io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Empty,
      org.snlab.proto.Ddmpt.Empty> getDumpMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Empty,
      org.snlab.proto.Ddmpt.Empty> getDumpMethod() {
    io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Empty, org.snlab.proto.Ddmpt.Empty> getDumpMethod;
    if ((getDumpMethod = RuntimeGrpc.getDumpMethod) == null) {
      synchronized (RuntimeGrpc.class) {
        if ((getDumpMethod = RuntimeGrpc.getDumpMethod) == null) {
          RuntimeGrpc.getDumpMethod = getDumpMethod = 
              io.grpc.MethodDescriptor.<org.snlab.proto.Ddmpt.Empty, org.snlab.proto.Ddmpt.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "org.snlab.proto.Runtime", "dump"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.snlab.proto.Ddmpt.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.snlab.proto.Ddmpt.Empty.getDefaultInstance()))
                  .setSchemaDescriptor(new RuntimeMethodDescriptorSupplier("dump"))
                  .build();
          }
        }
     }
     return getDumpMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getReplayMethod()} instead. 
  public static final io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Empty,
      org.snlab.proto.Ddmpt.Empty> METHOD_REPLAY = getReplayMethod();

  private static volatile io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Empty,
      org.snlab.proto.Ddmpt.Empty> getReplayMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Empty,
      org.snlab.proto.Ddmpt.Empty> getReplayMethod() {
    io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Empty, org.snlab.proto.Ddmpt.Empty> getReplayMethod;
    if ((getReplayMethod = RuntimeGrpc.getReplayMethod) == null) {
      synchronized (RuntimeGrpc.class) {
        if ((getReplayMethod = RuntimeGrpc.getReplayMethod) == null) {
          RuntimeGrpc.getReplayMethod = getReplayMethod = 
              io.grpc.MethodDescriptor.<org.snlab.proto.Ddmpt.Empty, org.snlab.proto.Ddmpt.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "org.snlab.proto.Runtime", "replay"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.snlab.proto.Ddmpt.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.snlab.proto.Ddmpt.Empty.getDefaultInstance()))
                  .setSchemaDescriptor(new RuntimeMethodDescriptorSupplier("replay"))
                  .build();
          }
        }
     }
     return getReplayMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static RuntimeStub newStub(io.grpc.Channel channel) {
    return new RuntimeStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static RuntimeBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new RuntimeBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static RuntimeFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new RuntimeFutureStub(channel);
  }

  /**
   */
  public static abstract class RuntimeImplBase implements io.grpc.BindableService {

    /**
     */
    public void process(org.snlab.proto.Ddmpt.Msg request,
        io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getProcessMethod(), responseObserver);
    }

    /**
     */
    public void setTask(org.snlab.proto.Ddmpt.Task request,
        io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getSetTaskMethod(), responseObserver);
    }

    /**
     */
    public void addVnode(org.snlab.proto.Ddmpt.Vnode request,
        io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getAddVnodeMethod(), responseObserver);
    }

    /**
     */
    public void addVnodes(org.snlab.proto.Ddmpt.Vnodes request,
        io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getAddVnodesMethod(), responseObserver);
    }

    /**
     */
    public void insertEntry(org.snlab.proto.Ddmpt.Entry request,
        io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getInsertEntryMethod(), responseObserver);
    }

    /**
     */
    public void showVroutes(org.snlab.proto.Ddmpt.Empty request,
        io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Vroutes> responseObserver) {
      asyncUnimplementedUnaryCall(getShowVroutesMethod(), responseObserver);
    }

    /**
     */
    public void verify(org.snlab.proto.Ddmpt.VerifyTask request,
        io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getVerifyMethod(), responseObserver);
    }

    /**
     */
    public void showVnodes(org.snlab.proto.Ddmpt.Empty request,
        io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Vnodes> responseObserver) {
      asyncUnimplementedUnaryCall(getShowVnodesMethod(), responseObserver);
    }

    /**
     */
    public void deleteRule(org.snlab.proto.Ddmpt.Empty request,
        io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getDeleteRuleMethod(), responseObserver);
    }

    /**
     */
    public void dump(org.snlab.proto.Ddmpt.Empty request,
        io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getDumpMethod(), responseObserver);
    }

    /**
     */
    public void replay(org.snlab.proto.Ddmpt.Empty request,
        io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getReplayMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getProcessMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                org.snlab.proto.Ddmpt.Msg,
                org.snlab.proto.Ddmpt.Empty>(
                  this, METHODID_PROCESS)))
          .addMethod(
            getSetTaskMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                org.snlab.proto.Ddmpt.Task,
                org.snlab.proto.Ddmpt.Empty>(
                  this, METHODID_SET_TASK)))
          .addMethod(
            getAddVnodeMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                org.snlab.proto.Ddmpt.Vnode,
                org.snlab.proto.Ddmpt.Empty>(
                  this, METHODID_ADD_VNODE)))
          .addMethod(
            getAddVnodesMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                org.snlab.proto.Ddmpt.Vnodes,
                org.snlab.proto.Ddmpt.Empty>(
                  this, METHODID_ADD_VNODES)))
          .addMethod(
            getInsertEntryMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                org.snlab.proto.Ddmpt.Entry,
                org.snlab.proto.Ddmpt.Empty>(
                  this, METHODID_INSERT_ENTRY)))
          .addMethod(
            getShowVroutesMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                org.snlab.proto.Ddmpt.Empty,
                org.snlab.proto.Ddmpt.Vroutes>(
                  this, METHODID_SHOW_VROUTES)))
          .addMethod(
            getVerifyMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                org.snlab.proto.Ddmpt.VerifyTask,
                org.snlab.proto.Ddmpt.Empty>(
                  this, METHODID_VERIFY)))
          .addMethod(
            getShowVnodesMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                org.snlab.proto.Ddmpt.Empty,
                org.snlab.proto.Ddmpt.Vnodes>(
                  this, METHODID_SHOW_VNODES)))
          .addMethod(
            getDeleteRuleMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                org.snlab.proto.Ddmpt.Empty,
                org.snlab.proto.Ddmpt.Empty>(
                  this, METHODID_DELETE_RULE)))
          .addMethod(
            getDumpMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                org.snlab.proto.Ddmpt.Empty,
                org.snlab.proto.Ddmpt.Empty>(
                  this, METHODID_DUMP)))
          .addMethod(
            getReplayMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                org.snlab.proto.Ddmpt.Empty,
                org.snlab.proto.Ddmpt.Empty>(
                  this, METHODID_REPLAY)))
          .build();
    }
  }

  /**
   */
  public static final class RuntimeStub extends io.grpc.stub.AbstractStub<RuntimeStub> {
    private RuntimeStub(io.grpc.Channel channel) {
      super(channel);
    }

    private RuntimeStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected RuntimeStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new RuntimeStub(channel, callOptions);
    }

    /**
     */
    public void process(org.snlab.proto.Ddmpt.Msg request,
        io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getProcessMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void setTask(org.snlab.proto.Ddmpt.Task request,
        io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSetTaskMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void addVnode(org.snlab.proto.Ddmpt.Vnode request,
        io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getAddVnodeMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void addVnodes(org.snlab.proto.Ddmpt.Vnodes request,
        io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getAddVnodesMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void insertEntry(org.snlab.proto.Ddmpt.Entry request,
        io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getInsertEntryMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void showVroutes(org.snlab.proto.Ddmpt.Empty request,
        io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Vroutes> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getShowVroutesMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void verify(org.snlab.proto.Ddmpt.VerifyTask request,
        io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getVerifyMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void showVnodes(org.snlab.proto.Ddmpt.Empty request,
        io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Vnodes> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getShowVnodesMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void deleteRule(org.snlab.proto.Ddmpt.Empty request,
        io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getDeleteRuleMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void dump(org.snlab.proto.Ddmpt.Empty request,
        io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getDumpMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void replay(org.snlab.proto.Ddmpt.Empty request,
        io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getReplayMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class RuntimeBlockingStub extends io.grpc.stub.AbstractStub<RuntimeBlockingStub> {
    private RuntimeBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private RuntimeBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected RuntimeBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new RuntimeBlockingStub(channel, callOptions);
    }

    /**
     */
    public org.snlab.proto.Ddmpt.Empty process(org.snlab.proto.Ddmpt.Msg request) {
      return blockingUnaryCall(
          getChannel(), getProcessMethod(), getCallOptions(), request);
    }

    /**
     */
    public org.snlab.proto.Ddmpt.Empty setTask(org.snlab.proto.Ddmpt.Task request) {
      return blockingUnaryCall(
          getChannel(), getSetTaskMethod(), getCallOptions(), request);
    }

    /**
     */
    public org.snlab.proto.Ddmpt.Empty addVnode(org.snlab.proto.Ddmpt.Vnode request) {
      return blockingUnaryCall(
          getChannel(), getAddVnodeMethod(), getCallOptions(), request);
    }

    /**
     */
    public org.snlab.proto.Ddmpt.Empty addVnodes(org.snlab.proto.Ddmpt.Vnodes request) {
      return blockingUnaryCall(
          getChannel(), getAddVnodesMethod(), getCallOptions(), request);
    }

    /**
     */
    public org.snlab.proto.Ddmpt.Empty insertEntry(org.snlab.proto.Ddmpt.Entry request) {
      return blockingUnaryCall(
          getChannel(), getInsertEntryMethod(), getCallOptions(), request);
    }

    /**
     */
    public org.snlab.proto.Ddmpt.Vroutes showVroutes(org.snlab.proto.Ddmpt.Empty request) {
      return blockingUnaryCall(
          getChannel(), getShowVroutesMethod(), getCallOptions(), request);
    }

    /**
     */
    public org.snlab.proto.Ddmpt.Empty verify(org.snlab.proto.Ddmpt.VerifyTask request) {
      return blockingUnaryCall(
          getChannel(), getVerifyMethod(), getCallOptions(), request);
    }

    /**
     */
    public org.snlab.proto.Ddmpt.Vnodes showVnodes(org.snlab.proto.Ddmpt.Empty request) {
      return blockingUnaryCall(
          getChannel(), getShowVnodesMethod(), getCallOptions(), request);
    }

    /**
     */
    public org.snlab.proto.Ddmpt.Empty deleteRule(org.snlab.proto.Ddmpt.Empty request) {
      return blockingUnaryCall(
          getChannel(), getDeleteRuleMethod(), getCallOptions(), request);
    }

    /**
     */
    public org.snlab.proto.Ddmpt.Empty dump(org.snlab.proto.Ddmpt.Empty request) {
      return blockingUnaryCall(
          getChannel(), getDumpMethod(), getCallOptions(), request);
    }

    /**
     */
    public org.snlab.proto.Ddmpt.Empty replay(org.snlab.proto.Ddmpt.Empty request) {
      return blockingUnaryCall(
          getChannel(), getReplayMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class RuntimeFutureStub extends io.grpc.stub.AbstractStub<RuntimeFutureStub> {
    private RuntimeFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private RuntimeFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected RuntimeFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new RuntimeFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<org.snlab.proto.Ddmpt.Empty> process(
        org.snlab.proto.Ddmpt.Msg request) {
      return futureUnaryCall(
          getChannel().newCall(getProcessMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<org.snlab.proto.Ddmpt.Empty> setTask(
        org.snlab.proto.Ddmpt.Task request) {
      return futureUnaryCall(
          getChannel().newCall(getSetTaskMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<org.snlab.proto.Ddmpt.Empty> addVnode(
        org.snlab.proto.Ddmpt.Vnode request) {
      return futureUnaryCall(
          getChannel().newCall(getAddVnodeMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<org.snlab.proto.Ddmpt.Empty> addVnodes(
        org.snlab.proto.Ddmpt.Vnodes request) {
      return futureUnaryCall(
          getChannel().newCall(getAddVnodesMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<org.snlab.proto.Ddmpt.Empty> insertEntry(
        org.snlab.proto.Ddmpt.Entry request) {
      return futureUnaryCall(
          getChannel().newCall(getInsertEntryMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<org.snlab.proto.Ddmpt.Vroutes> showVroutes(
        org.snlab.proto.Ddmpt.Empty request) {
      return futureUnaryCall(
          getChannel().newCall(getShowVroutesMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<org.snlab.proto.Ddmpt.Empty> verify(
        org.snlab.proto.Ddmpt.VerifyTask request) {
      return futureUnaryCall(
          getChannel().newCall(getVerifyMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<org.snlab.proto.Ddmpt.Vnodes> showVnodes(
        org.snlab.proto.Ddmpt.Empty request) {
      return futureUnaryCall(
          getChannel().newCall(getShowVnodesMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<org.snlab.proto.Ddmpt.Empty> deleteRule(
        org.snlab.proto.Ddmpt.Empty request) {
      return futureUnaryCall(
          getChannel().newCall(getDeleteRuleMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<org.snlab.proto.Ddmpt.Empty> dump(
        org.snlab.proto.Ddmpt.Empty request) {
      return futureUnaryCall(
          getChannel().newCall(getDumpMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<org.snlab.proto.Ddmpt.Empty> replay(
        org.snlab.proto.Ddmpt.Empty request) {
      return futureUnaryCall(
          getChannel().newCall(getReplayMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_PROCESS = 0;
  private static final int METHODID_SET_TASK = 1;
  private static final int METHODID_ADD_VNODE = 2;
  private static final int METHODID_ADD_VNODES = 3;
  private static final int METHODID_INSERT_ENTRY = 4;
  private static final int METHODID_SHOW_VROUTES = 5;
  private static final int METHODID_VERIFY = 6;
  private static final int METHODID_SHOW_VNODES = 7;
  private static final int METHODID_DELETE_RULE = 8;
  private static final int METHODID_DUMP = 9;
  private static final int METHODID_REPLAY = 10;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final RuntimeImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(RuntimeImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_PROCESS:
          serviceImpl.process((org.snlab.proto.Ddmpt.Msg) request,
              (io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty>) responseObserver);
          break;
        case METHODID_SET_TASK:
          serviceImpl.setTask((org.snlab.proto.Ddmpt.Task) request,
              (io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty>) responseObserver);
          break;
        case METHODID_ADD_VNODE:
          serviceImpl.addVnode((org.snlab.proto.Ddmpt.Vnode) request,
              (io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty>) responseObserver);
          break;
        case METHODID_ADD_VNODES:
          serviceImpl.addVnodes((org.snlab.proto.Ddmpt.Vnodes) request,
              (io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty>) responseObserver);
          break;
        case METHODID_INSERT_ENTRY:
          serviceImpl.insertEntry((org.snlab.proto.Ddmpt.Entry) request,
              (io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty>) responseObserver);
          break;
        case METHODID_SHOW_VROUTES:
          serviceImpl.showVroutes((org.snlab.proto.Ddmpt.Empty) request,
              (io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Vroutes>) responseObserver);
          break;
        case METHODID_VERIFY:
          serviceImpl.verify((org.snlab.proto.Ddmpt.VerifyTask) request,
              (io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty>) responseObserver);
          break;
        case METHODID_SHOW_VNODES:
          serviceImpl.showVnodes((org.snlab.proto.Ddmpt.Empty) request,
              (io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Vnodes>) responseObserver);
          break;
        case METHODID_DELETE_RULE:
          serviceImpl.deleteRule((org.snlab.proto.Ddmpt.Empty) request,
              (io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty>) responseObserver);
          break;
        case METHODID_DUMP:
          serviceImpl.dump((org.snlab.proto.Ddmpt.Empty) request,
              (io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty>) responseObserver);
          break;
        case METHODID_REPLAY:
          serviceImpl.replay((org.snlab.proto.Ddmpt.Empty) request,
              (io.grpc.stub.StreamObserver<org.snlab.proto.Ddmpt.Empty>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class RuntimeBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    RuntimeBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return org.snlab.proto.Ddmpt.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("Runtime");
    }
  }

  private static final class RuntimeFileDescriptorSupplier
      extends RuntimeBaseDescriptorSupplier {
    RuntimeFileDescriptorSupplier() {}
  }

  private static final class RuntimeMethodDescriptorSupplier
      extends RuntimeBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    RuntimeMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (RuntimeGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new RuntimeFileDescriptorSupplier())
              .addMethod(getProcessMethod())
              .addMethod(getSetTaskMethod())
              .addMethod(getAddVnodeMethod())
              .addMethod(getAddVnodesMethod())
              .addMethod(getInsertEntryMethod())
              .addMethod(getShowVroutesMethod())
              .addMethod(getVerifyMethod())
              .addMethod(getShowVnodesMethod())
              .addMethod(getDeleteRuleMethod())
              .addMethod(getDumpMethod())
              .addMethod(getReplayMethod())
              .build();
        }
      }
    }
    return result;
  }
}
