package org.snlab.proto;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.9.1)",
    comments = "Source: ddmpt.proto")
public final class NotifyGrpc {

  private NotifyGrpc() {}

  public static final String SERVICE_NAME = "org.snlab.proto.Notify";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getProcessMethod()} instead. 
  public static final io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Msg,
      com.google.protobuf.Empty> METHOD_PROCESS = getProcessMethod();

  private static volatile io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Msg,
      com.google.protobuf.Empty> getProcessMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Msg,
      com.google.protobuf.Empty> getProcessMethod() {
    io.grpc.MethodDescriptor<org.snlab.proto.Ddmpt.Msg, com.google.protobuf.Empty> getProcessMethod;
    if ((getProcessMethod = NotifyGrpc.getProcessMethod) == null) {
      synchronized (NotifyGrpc.class) {
        if ((getProcessMethod = NotifyGrpc.getProcessMethod) == null) {
          NotifyGrpc.getProcessMethod = getProcessMethod = 
              io.grpc.MethodDescriptor.<org.snlab.proto.Ddmpt.Msg, com.google.protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "org.snlab.proto.Notify", "process"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.snlab.proto.Ddmpt.Msg.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
                  .setSchemaDescriptor(new NotifyMethodDescriptorSupplier("process"))
                  .build();
          }
        }
     }
     return getProcessMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static NotifyStub newStub(io.grpc.Channel channel) {
    return new NotifyStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static NotifyBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new NotifyBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static NotifyFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new NotifyFutureStub(channel);
  }

  /**
   */
  public static abstract class NotifyImplBase implements io.grpc.BindableService {

    /**
     */
    public void process(org.snlab.proto.Ddmpt.Msg request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getProcessMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getProcessMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                org.snlab.proto.Ddmpt.Msg,
                com.google.protobuf.Empty>(
                  this, METHODID_PROCESS)))
          .build();
    }
  }

  /**
   */
  public static final class NotifyStub extends io.grpc.stub.AbstractStub<NotifyStub> {
    private NotifyStub(io.grpc.Channel channel) {
      super(channel);
    }

    private NotifyStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected NotifyStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new NotifyStub(channel, callOptions);
    }

    /**
     */
    public void process(org.snlab.proto.Ddmpt.Msg request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getProcessMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class NotifyBlockingStub extends io.grpc.stub.AbstractStub<NotifyBlockingStub> {
    private NotifyBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private NotifyBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected NotifyBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new NotifyBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.google.protobuf.Empty process(org.snlab.proto.Ddmpt.Msg request) {
      return blockingUnaryCall(
          getChannel(), getProcessMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class NotifyFutureStub extends io.grpc.stub.AbstractStub<NotifyFutureStub> {
    private NotifyFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private NotifyFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected NotifyFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new NotifyFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> process(
        org.snlab.proto.Ddmpt.Msg request) {
      return futureUnaryCall(
          getChannel().newCall(getProcessMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_PROCESS = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final NotifyImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(NotifyImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_PROCESS:
          serviceImpl.process((org.snlab.proto.Ddmpt.Msg) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class NotifyBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    NotifyBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return org.snlab.proto.Ddmpt.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("Notify");
    }
  }

  private static final class NotifyFileDescriptorSupplier
      extends NotifyBaseDescriptorSupplier {
    NotifyFileDescriptorSupplier() {}
  }

  private static final class NotifyMethodDescriptorSupplier
      extends NotifyBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    NotifyMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (NotifyGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new NotifyFileDescriptorSupplier())
              .addMethod(getProcessMethod())
              .build();
        }
      }
    }
    return result;
  }
}
