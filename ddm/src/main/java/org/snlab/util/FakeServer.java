package org.snlab.util;

import java.io.IOException;

import org.snlab.proto.Ddmpt.Empty;
import org.snlab.proto.Ddmpt.Msg;
import org.snlab.proto.RuntimeGrpc.RuntimeImplBase;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;

public class FakeServer {
    public static Server server;

    public static void main(String[] args) throws InterruptedException {
        if (args.length == 0) {
            return;
        }
        Server server = ServerBuilder.forPort(Integer.valueOf(args[0])).addService(new FakeRuntime())
                .build();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                FakeServer.server.shutdown();
            }
        });
        try {
            server.start();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            FakeServer.server.shutdown();
            System.exit(1);
        }
        server.awaitTermination();
    }
}

class FakeRuntime extends RuntimeImplBase {
    @Override
    public void process(Msg request, StreamObserver<Empty> responseObserver) {
        responseObserver.onNext(Empty.newBuilder().build());
        responseObserver.onCompleted();
    }
}