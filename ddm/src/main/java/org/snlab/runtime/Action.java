package org.snlab.runtime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Action {
    public enum Mode {ANY, ALL}
    
    private Mode mode = Mode.ALL;
    private Set<String> ports = new HashSet<>();

    public Action(String port) {
        this.mode = Mode.ALL;
        this.ports.add(port);
    }

    public Action(Set<String> ports, Mode mode) {
        this.ports = ports;
        this.mode = mode;
    }

    public Mode getMode() {
        return mode;
    }

    public Set<String> getPorts() {
        return ports;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Action) {
            Action that = (Action)obj;
            if (this.mode == that.mode && this.ports.equals(that.ports)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return mode.hashCode() + ports.hashCode(); // may conflict?
    }

    public static void main(String[] args) {
        Port p1 = new Port("p1");
        Port p2 = new Port("p2");
        Set<Port> ports = new HashSet<>();
        ports.add(p1);
        ports.add(p2);
        Set<String> pts1 = new HashSet<>();
        pts1.add("p1");
        pts1.add("p2");
        
        Set<String> pts2 = new HashSet<>();
        pts2.add("p1");
        pts2.add("p2");
        Action x = new Action(pts1, Mode.ALL);
        Set<Port> ports1 = new HashSet<>();
        ports1.add(p1);
        ports1.add(p2);
        Action y = new Action(pts2, Mode.ALL);
        Map<Action, Integer> actionToPred = new HashMap<>();
        actionToPred.put(x, 1);
        System.out.println(x.equals(y));
        System.out.println(actionToPred.containsKey(y));
    }
}
