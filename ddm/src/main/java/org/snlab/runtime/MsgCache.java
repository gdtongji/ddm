package org.snlab.runtime;

import java.util.HashSet;
import java.util.Set;

import org.snlab.proto.Ddmpt.Msg;

public class MsgCache {
    private Set<Msg> msgs = new HashSet<>();

    public void save(Msg msg) {
        msgs.add(msg);
    }
}
