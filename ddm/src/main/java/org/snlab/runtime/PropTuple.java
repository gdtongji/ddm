package org.snlab.runtime;

public class PropTuple<T> {
    private int hs;
    private T prop;

    public PropTuple() {}

    public PropTuple(int hs, T prop) {
        this.hs = hs;
        this.prop = prop;
    }

    public int getHs() {
        return this.hs;
    }

    public T getProp() {
        return this.prop;
    }

    /**
     * @param hs the hs to set
     */
    public void setHs(int hs) {
        this.hs = hs;
    }

    /**
     * @param prop the prop to set
     */
    public void setProp(T prop) {
        this.prop = prop;
    }

}
