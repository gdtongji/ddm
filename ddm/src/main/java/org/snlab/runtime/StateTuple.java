package org.snlab.runtime;

public class StateTuple<T> {
    private int hs;
    private T state;

    public StateTuple(int hs, T state) {
        this.hs = hs;
        this.state = state;
    }

    public int getHs() {
        return this.hs;
    }

    public T getState() {
        return this.state;
    }
}
