package org.snlab.runtime;

public class Port {
    private String name;

    public Port(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
