package org.snlab.runtime;


public class Rule {
    private int prefix;
    private int hit;
    private String port;
    private Action action;
    private int matchBdd;

    public Rule(int hit, int prefix, String port) {
        this.hit = hit;
        this.prefix = prefix;
        this.port = port;
    }

    public Rule(int hit, int prefix, Action action) {
        this.hit = hit;
        this.prefix = prefix;
        this.action = action;
    }

    /**
     * @return int return the prefix
     */
    public int getPrefix() {
        return prefix;
    }

    /**
     * @param prefix the prefix to set
     */
    public void setPrefix(int prefix) {
        this.prefix = prefix;
    }

    /**
     * @return int return the hit
     */
    public int getHit() {
        return hit;
    }

    /**
     * @param hit the hit to set
     */
    public void setHit(int hit) {
        this.hit = hit;
    }

    /**
     * @return String return the port
     */
    public String getPort() {
        return port;
    }

    /**
     * @param port the port to set
     */
    public void setPort(String port) {
        this.port = port;
    }


    /**
     * @return Action return the action
     */
    public Action getAction() {
        return action;
    }

    /**
     * @param action the action to set
     */
    public void setAction(Action action) {
        this.action = action;
    }


    /**
     * @return int return the matchBdd
     */
    public int getMatchBdd() {
        return matchBdd;
    }

    /**
     * @param matchBdd the matchBdd to set
     */
    public void setMatchBdd(int matchBdd) {
        this.matchBdd = matchBdd;
    }

}
