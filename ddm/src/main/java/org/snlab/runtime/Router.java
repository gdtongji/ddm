package org.snlab.runtime;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.snlab.proto.Ddmpt.Nexthop;

/**
 * Route received msg to correct vnode
 */
public class Router {
    private Map<Long, List<Nexthop>> table = new HashMap<>();

    public List<Nexthop> route(Long vid) {
        return this.table.get(vid);
    }

    public void insert(Long key, List<Nexthop> nexthops) {
        this.table.put(key, nexthops);
    }

    /**
     * @return Map<Long, List<Nexthop>> return the table
     */
    public Map<Long, List<Nexthop>> getTable() {
        return table;
    }

    /**
     * @param table the table to set
     */
    public void setTable(Map<Long, List<Nexthop>> table) {
        this.table = table;
    }

}
