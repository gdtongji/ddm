package org.snlab.runtime;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Scanner;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.checkerframework.checker.units.qual.s;
import org.snlab.interfaces.TaskInf;
import org.snlab.util.Replay;

public class Daemon implements Runnable {
    // private static ThreadLocal<Config> cfg = new ThreadLocal<>();
    private Config config;
    private List<TaskInf> tasks;
    private DIB dib = new DIB();
    // private static ThreadLocal<Router> localRouter = ThreadLocal.withInitial(() -> new Router());
    private Router router = new Router();
    private Vrouter vrouter = new Vrouter();
    public static Logger logger = new Logger("output/log.txt");

    public Daemon(Config config) {
        // cfg.set(config); // we need to set thread local vars at run(), since the constructor is in Main thread
        this.config = config;
        this.vrouter.setDaemon(this);
        if (Replay.isReplay) {
            this.vrouter.loadTrace();
        }
        this.tasks = new ArrayList<>();
    }

    public Config getConfig() {
        return config;
    }

    public DIB getDIB() {
        return dib;
    }

    public Router getRouter() {
        return router;
    }

    public Vrouter getVrouter() {
        return vrouter;
    }

    public void addTask(TaskInf task) {
        this.tasks.add(task);
    }

    public BDDEngine getBddEngine() {
        return dib.getBDDEngine();
    }

    @Override
    public void run() {
        // cfg.set(this.config);
        // dib.set(new DIB(config.getName()));
        dib.setDeviceName(config.getName());
        dib.loadFib(config.getFib());
        // localRouter.set(new Router());
        long s = System.nanoTime();
        dib.computeECx();
        System.out.println(config.getName() + " ec build time: " + (System.nanoTime() - s));
        GRPCServer grpcServer = new GRPCServer(this);
        try {
            grpcServer.start();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void loadFunction(String... input_file)
            throws ClassNotFoundException, NoSuchFieldException, SecurityException {
        for (String fn : input_file) {
            JarFile jarFile;
            try {
                jarFile = new JarFile(fn);
                Enumeration<JarEntry> e = jarFile.entries();

                URL[] urls = { new URL("jar:file:" + fn + "!/") };
                URLClassLoader cl = URLClassLoader.newInstance(urls);

                while (e.hasMoreElements()) {
                    JarEntry je = e.nextElement();
                    if (je.isDirectory() || !je.getName().endsWith(".class")) {
                        continue;
                    }
                    // -6 because of .class
                    String className = je.getName().substring(0, je.getName().length() - 6);
                    className = className.replace('/', '.');
                    Class c = cl.loadClass(className);
                    Field tf = c.getField("transFn");
                    Field af = c.getField("aggrFn");
                    Field pd = c.getField("pred");
                    // TODO: create and inject functions to task

                }
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
    }

}
