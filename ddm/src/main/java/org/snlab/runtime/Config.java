package org.snlab.runtime;

import java.util.List;

public class Config {
    private String name;
    private int port;
    private String fib;
    private List<NeighborConfig> neighbors;
    private String envDir;

    /**
     * @return String return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return int return the port
     */
    public int getPort() {
        return port;
    }

    /**
     * @param port the port to set
     */
    public void setPort(int port) {
        this.port = port;
    }


    /**
     * @return List<Neighbor> return the neighbors
     */
    public List<NeighborConfig> getNeighbors() {
        return neighbors;
    }

    /**
     * @param neighbors the neighbors to set
     */
    public void setNeighbors(List<NeighborConfig> neighbors) {
        this.neighbors = neighbors;
    }


    /**
     * @return String return the fib
     */
    public String getFib() {
        return fib;
    }

    /**
     * @param fib the fib to set
     */
    public void setFib(String fib) {
        this.fib = fib;
    }


    /**
     * @return String return the envDir
     */
    public String getEnvDir() {
        return envDir;
    }

    /**
     * @param envDir the envDir to set
     */
    public void setEnvDir(String envDir) {
        this.envDir = envDir;
    }

}
