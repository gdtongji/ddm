package org.snlab.runtime;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class Main {
    public static void main(String[] args) throws Exception {
        Options options = new Options();
        options.addOption("n", true, "Device name");
        options.addOption("p", true, "The gRPC port");
        options.addOption("f", true, "The initial FIB");
        options.addOption("c", true, "The ddmd conf file");
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;
        try {
            cmd = parser.parse(options, args);
            Config config = new Config();
            if (cmd.hasOption("c")) {
                File file = new File(cmd.getOptionValue("c"));
                ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
                config = objectMapper.readValue(file, Config.class);
                System.out.println("Using config " + cmd.getOptionValue("c"));
            } else {
                config.setName(cmd.getOptionValue("n"));
                config.setPort(Integer.valueOf(cmd.getOptionValue("p")));
                config.setFib(cmd.getOptionValue("f"));
            }
            Daemon daemon = new Daemon(config);
            // daemon.setDaemon(true);
            daemon.run();
        } catch (ParseException e) {
            System.err.println(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        // String s = "";
        // while (!s.equals("exit")) {
        //     Scanner sc = new Scanner(System.in);
        //     s = sc.nextLine();
        // }
        System.out.println("eof");
    }
}
