package org.snlab.runtime;

import java.io.FileWriter;
import java.io.IOException;

import org.checkerframework.checker.units.qual.s;

public class Logger {
    private FileWriter writer;

    public Logger(String logFile) {
        try {
            writer = new FileWriter(logFile);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public synchronized void log(String s) {
        try {
            writer.write(System.currentTimeMillis() + " # " + s + "\n");
            writer.flush();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public synchronized void log(String fn, String s) {
        try (FileWriter writer = new FileWriter(fn, true)) {
            writer.append(s + "\n");
            writer.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
