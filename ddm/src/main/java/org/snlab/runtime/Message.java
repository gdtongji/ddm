package org.snlab.runtime;

import org.snlab.proto.Ddmpt.Msg;

public class Message {
    private Msg msg;
    private int hs;

    public Message(Msg msg, int hs) {
        this.msg = msg;
        this.hs = hs;
    }

    /**
     * @return Msg return the msg
     */
    public Msg getMsg() {
        return msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(Msg msg) {
        this.msg = msg;
    }

    /**
     * @return int return the hs
     */
    public int getHs() {
        return hs;
    }

    /**
     * @param hs the hs to set
     */
    public void setHs(int hs) {
        this.hs = hs;
    }

}
