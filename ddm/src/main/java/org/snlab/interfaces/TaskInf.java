package org.snlab.interfaces;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

import org.snlab.runtime.StateTuple;

/**
 * T: the generic state type
 */
public interface TaskInf<T> {
    void setDFAId(int id);

    void addDownstreams(String... downs);

    void setAttributeFn(Function<LinkInf, T> fn); // Tij

    void setTransitionFn(Function<T, T> fn); // P

    void setPredicate(Predicate<T> p); // V

    void process(List<StateTuple<T>> stateTuples);
}
