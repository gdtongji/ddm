package org.snlab.interfaces;

public interface RuleInf {
    String[] getNextHops();

    int getType(); // 0: unicast, 1: anycast, 2: multicast

    int getHit();
}
