package org.snlab.interfaces;

import org.jgrapht.Graph;

public interface CompilerInf {
    /**
     * compile a req string to injectable format, JVM byte code or a jar
     * 
     * @param req
     * @return
     */
    byte[] compile(Graph graph, String... reqs);

    void compile(Graph graph, String output, String... reqs);
}
