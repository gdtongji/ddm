package org.snlab.interfaces;

public interface DaemonInf {
    void configure();
    /**
     * Start daemon with a TCP server
     * @param tcp port
     */
    void start(int port);
    void stop();
    void addTask(TaskInf task);
}
