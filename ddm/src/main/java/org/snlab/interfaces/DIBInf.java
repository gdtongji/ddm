package org.snlab.interfaces;

/**
 * Device information base interface
 */
public interface DIBInf {
    String getDeviceName();

    void computeEC();

    void addRule();

    void dump();
}
