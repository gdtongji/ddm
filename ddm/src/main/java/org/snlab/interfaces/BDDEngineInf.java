package org.snlab.interfaces;

import java.math.BigInteger;

public interface BDDEngineInf {
    int and(int x, int y);

    int or(int x, int y);

    int not(int x);

    int encodeIP(int ip, int prefix);

    int encodeIP(BigInteger ip, int prefix);

    byte[] serialize(int x);

    int deserialize(byte[] bytes);
}
