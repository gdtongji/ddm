package org.snlab.interfaces;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedAcyclicGraph;

public interface DFAGenInf {
    DirectedAcyclicGraph<String, DefaultEdge> genDFA(Graph<String, DefaultEdge> topo,
            DirectedAcyclicGraph<Integer, DefaultEdge> automata);
}
