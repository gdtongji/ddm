package org.snlab;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

import org.snlab.runtime.BDDEngine;
import org.snlab.runtime.Daemon;
import org.snlab.runtime.StateTuple;
import org.snlab.runtime.Task;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        BDDEngine bddEngine = new BDDEngine(32);

        int x = bddEngine.encodeIP(Long.valueOf(720896), 16);
        for (int i = 0; i < 10; i++) {
            long s = System.nanoTime();
            byte[] y = bddEngine.serialize(x);
            System.out.println("seri: " + (System.nanoTime() - s) + " " + y.length);
            s = System.nanoTime();
            bddEngine.deserialize(y);
            System.out.println("deseri: " + (System.nanoTime() - s));
        }
        // Daemon daemon = new Daemon();

        // Function<Integer, Integer> tf = (x) -> 1;
        // Function<List<Integer>, Integer> af = (x) -> x.stream().reduce(0, Integer::sum);
        // Task<Integer> task = new Task<>();
        // task.setTransitionFn(tf);
        // task.setAggrFn(af);
        // task.setDFAId(0);
        // Predicate<Integer> p = (x) -> (x >= 1);
        // task.setPredicate(p);
        // daemon.addTask(task);
        // List<StateTuple<Integer>> stateTuples = new ArrayList<>();
        // task.process(stateTuples);
    }
}
