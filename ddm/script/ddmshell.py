from cmd import Cmd

class Ddmshell(Cmd):
    prompt = 'ddmshell> '
    def do_dump(self):
        """dd"""
        print('dump')

    def do_exit(self, line):
        exit(0)

sh = Ddmshell().cmdloop()