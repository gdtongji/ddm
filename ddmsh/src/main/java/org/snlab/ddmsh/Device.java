package org.snlab.ddmsh;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Device {
    private String name;
    private String addr;
    private Set<DFANode> vnodes = new HashSet<>();

    public Device(String name) {
        this.name = name;
    }

    public void addVnode(DFANode node) {
        this.vnodes.add(node);
    }

    /**
     * @return String return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return Set<DFANode> return the vnodes
     */
    public Set<DFANode> getVnodes() {
        return vnodes;
    }

    /**
     * @param vnodes the vnodes to set
     */
    public void setVnodes(Set<DFANode> vnodes) {
        this.vnodes = vnodes;
    }


    /**
     * @return String return the addr
     */
    public String getAddr() {
        return addr;
    }

    /**
     * @param addr the addr to set
     */
    public void setAddr(String addr) {
        this.addr = addr;
    }

}
