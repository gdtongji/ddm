package org.snlab.ddmsh;

import java.util.HashMap;
import java.util.Map;

import org.snlab.proto.RuntimeGrpc.RuntimeBlockingStub;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Ddmsh {
	public static Map<String, String> deviceToAddr = new HashMap<>();
	public static Map<Long, DFANode> vidToDFANode = new HashMap<>();
	public static Map<String, DFA> nameToDFA = new HashMap<>();
	public static Map<String, RuntimeBlockingStub> deviceToStub = new HashMap<>();

	public static void main(String[] args) {
		SpringApplication.run(Ddmsh.class, args);
	}

}
