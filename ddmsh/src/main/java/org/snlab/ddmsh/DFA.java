package org.snlab.ddmsh;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

public class DFA {
    private String name;
    private Graph<DFANode, TransferEdge> graph = new DefaultDirectedGraph<>(TransferEdge.class);
    private DFANode start;
    private DFANode accept;
    private Map<String, DFANode> nameToNode = new HashMap<>();

    public static DFA fromFile(File f) {
        DFA dfa = new DFA();
        dfa.setName(f.getName());
        Scanner in = null;
        try {
            in = new Scanner(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        while (in.hasNextLine()) {
            String line = in.nextLine();
            String[] tokens = line.split(" ");
            DFANode src = dfa.getOrCreateNode(tokens[0], 0);
            DFANode dst = dfa.getOrCreateNode(tokens[1], 0);
            
            TransferEdge edge = new TransferEdge(tokens[1]);
            dfa.getGraph().addEdge(src, dst, edge);
        }
        return dfa;
    }

    public static DFA fromFileOpt(File f) {
        DFA dfa = new DFA();
        dfa.setName(f.getName());
        Scanner in = null;
        try {
            in = new Scanner(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        while (in.hasNextLine()) {
            String line = in.nextLine();
            String[] tokens = line.split(" ");
            
            DFANode src = dfa.getOrCreateNode(tokens[0].split(":")[0], Integer.valueOf(tokens[0].split(":")[1]));
            DFANode dst = dfa.getOrCreateNode(tokens[1].split(":")[0], Integer.valueOf(tokens[1].split(":")[1]));
            TransferEdge edge;
            if (tokens.length > 2) {
                edge = new TransferEdge(tokens[2]);
            } else {
                edge = new TransferEdge(tokens[1]);
            }
            dfa.getGraph().addEdge(src, dst, edge);
        }
        return dfa;
    }

    public DFANode getOrCreateNode(String device, int index) {
        if (nameToNode.containsKey(device + String.valueOf(index))) {
            return nameToNode.get(device + String.valueOf(index));
        }
        DFANode node = new DFANode(device, this);
        node.setIndex(index);
        nameToNode.put(device + String.valueOf(index), node);
        this.graph.addVertex(node);
        Ddmsh.vidToDFANode.put(node.getUuid(), node);
        return node;
    }

    public void newState(DFANode state) {
        this.graph.addVertex(state);
    }

    public void newStates(Set<DFANode> states) {
        for (DFANode s : states) {
            this.newState(s);
        }
    }

    public void newTransfer(DFANode from, DFANode to, String symbol) {
        this.graph.addEdge(from, to, new TransferEdge(symbol));
    }

    public void setStart(DFANode state) {
        this.start = state;
    }

    public void setAccept(DFANode state) {
        this.accept = state;
    }

    /**
     * @return Graph<DFANode, DefaultEdge> return the graph
     */
    public Graph<DFANode, TransferEdge> getGraph() {
        return graph;
    }

    /**
     * @param graph the graph to set
     */
    public void setGraph(Graph<DFANode, TransferEdge> graph) {
        this.graph = graph;
    }

    /**
     * @return DFANode return the start
     */
    public DFANode getStart() {
        return start;
    }

    /**
     * @return DFANode return the accept
     */
    public DFANode getAccept() {
        return accept;
    }

    /**
     * @return String return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

}

class TransferEdge extends DefaultEdge {
    private String symbol;

    public TransferEdge(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return this.symbol;
    }

    /**
     * @param symbol the symbol to set
     */
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }


}