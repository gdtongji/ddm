package org.snlab.ddmsh;

import java.util.List;

public class DdmdConfs {
    private List<DdmdConf> confs;

    /**
     * @return List<DdmdConf> return the confs
     */
    public List<DdmdConf> getDdmdConfs() {
        return confs;
    }

    /**
     * @param ddmdConfs the confs to set
     */
    public void setDdmdConfs(List<DdmdConf> confs) {
        this.confs = confs;
    }

}
