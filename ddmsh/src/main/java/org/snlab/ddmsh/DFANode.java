package org.snlab.ddmsh;

import com.fasterxml.uuid.Generators;

public class DFANode {
    // assign an uuid for each node of DFA to forbit name conflict
    private long uuid = Generators.timeBasedGenerator().generate().node();
    private String device;
    private int index = 0;
    private DFA dfa; // pointer to belonging DFA

    public DFANode(String device, DFA dfa) {
        this.device = device;
        this.dfa = dfa;
    }

    @Override
    public boolean equals(Object arg0) {
        return this.device == ((DFANode)arg0).getDevice() && this.index == ((DFANode)arg0).getIndex();
    }

    @Override
    public int hashCode() {
        return device.hashCode() + index;
    }

    public DFA getDFA() {
        return this.dfa;
    }

    /**
     * @return long return the uuid
     */
    public long getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(long uuid) {
        this.uuid = uuid;
    }

    /**
     * @return Device return the device
     */
    public String getDevice() {
        return device;
    }

    /**
     * @param device the device to set
     */
    public void setDevice(String device) {
        this.device = device;
    }

    /**
     * @return int return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * @param index the index to set
     */
    public void setIndex(int index) {
        this.index = index;
    }

}
