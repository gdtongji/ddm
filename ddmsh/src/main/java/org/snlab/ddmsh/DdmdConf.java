package org.snlab.ddmsh;

public class DdmdConf {
    private String name;
    private int port;
    private String fib;

    /**
     * @return String return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return int return the port
     */
    public int getPort() {
        return port;
    }

    /**
     * @param port the port to set
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * @return String return the fib
     */
    public String getFib() {
        return fib;
    }

    /**
     * @param fib the fib to set
     */
    public void setFib(String fib) {
        this.fib = fib;
    }

}
