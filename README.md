# Distributed Dataplane Monitor

transF: T x DIB -> T  f(T V, DIB dib) {min(v[1], dib.match(v[0]).bw)}
aggrF: T -> T
predF: T x DIB -> Bool f(T V, DIB dib) {V[0] in DIB.out(V[0])}

dfa = gen()
VEC<hs, bw, latency, r> vec
vec.filter().map(bw).reduce(hs@union, latency@max).select(bw).verify(forall bw > 100)

R : bw > 100
vec.select(bw).verify(forall bw > 100)

preGenerate(vec):
trans: vec -> vec['r'] = R(vec[bw])
vec.filter().map(trans).reduce(hs@union, latency@max, r@equel).count(r=0).

postGenerate(vec):
vec.filter().
```
ddm.createSchema({
    name: 'myschema',
    properties: {
        hs: HS,
        bw: int,
        latency: int
    },
    map: function(data) {
        for (e in data) {
            emit(e.bw, {hs: e.hs})
        }
    },
    reduce: function(key, emits) {
        return {hs: UNION(emits.hs)}
    },
    verify: function(emits) {
        for (e in emits) {
            e.bw >= 100
        }
    },
    transfer: functioin(pros, E) {
        emit((pros.hs ^ E.hs, MIN(vec.bw, E.bw), vec.latency + E.latency))
    }
})
```
```
DEFINE FORWARD_DFA shortest_path
FILTER S,.*D
AS dfa1

DEFINE r AS bw > 100 AND hspace == {dstip=192.168.0.0/24} AND latency <= 100

QUERY path IN dfa1 FOR {dstip=192.168.0.0/24}
SELECT r, hspace, bw, latency
WHERE r == TRUE
AGGREGATE hspace BY union
AGGREGATE bw BY min
AGGREGATE latency by max
AGGREGATE r BY AND/OR
```

```
python3 envs/fb/gen.py -f envs/fb
//python3 scripts/dfa_generator.py envs/fb/topo.txt rsw-0-0 rsw-1-0 envs/fb/gen/dfas/dfa.txt
python .\scripts\offline.py -r -k 112 envs/fb2
python .\scripts\offline.py -fb -k 2 envs/fb2
python3 scripts/simnet.py -g -p 20000 envs/fb
java -Xms30g -Xmx110g -jar simnet.jar -e envs/fb
java -jar ddmc.jar -t envs/fb/topo.txt -o envs/fb/ -d envs/fb/ddmdconfs.yaml -dfa envs/fb/dfa.txt

```
ddmsh:
```
.\mvnw clean install -DskipTests
java -jar .\target\ddmsh-0.0.1-SNAPSHOT.jar
```

fb single-pair DFA: #states: 202, #trans: 392
opted DFA src-all dst: #states: 6016 #trans: 

noptstate=3*48*k + 49*4*k + 192*k
