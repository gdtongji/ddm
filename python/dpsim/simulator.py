import simpy
import random
import statistics
import confuse

from .network import NetworkProvider

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Simulator for Data Plane Verification')
    parser.add_argument('-c', dest='config_file',
                        type=str, default='config.yaml',
                        help='Path to the configuration file')

    args = parser.parse_args()

    config = confuse.Configuration("dpsim", __name__)
    config.set_file(args.config_file)

    network = NetworkProvider(config['network'])

    env = simpy.Environment()

    network.configure(env)

    network.draw('test.pdf')

    env.process(network.start())

    env.run()

    network.dump_fib()
