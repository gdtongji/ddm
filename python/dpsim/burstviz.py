import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

def plot_burst(in_file, out_file):
    sns.set()

    df = pd.read_csv(in_file)
    df['time'] = (df['timestamp'].astype('int') / 10).astype('int') * 100
    fib_burst = df.groupby(['time', 'dev']).size().reset_index(name='count')

    sns.scatterplot(data=fib_burst, x='time', y='dev', hue='count')

    plt.tight_layout()
    plt.savefig(out_file)

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Visualizing FIB bursts')
    parser.add_argument('in_file', metavar='FIN',
                        type=str,
                        help='Path to the configuration file')
    parser.add_argument('out_file', metavar='FOUT',
                        type=str,
                        help='Path to the configuration file')

    args = parser.parse_args()

    plot_burst(args.in_file, args.out_file)
