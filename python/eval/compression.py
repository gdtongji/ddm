import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

import matplotlib
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='DFA compression for Fat tree')
    parser.add_argument('fin', metavar='IN',
                        type=str,
                        help='Path to the source .csv file')
    parser.add_argument('fout', metavar='OUT',
                        type=str,
                        help='Path to the target .pdf file')

    args = parser.parse_args()

    df = pd.read_csv(args.fin)

    plt.figure(figsize=(.8 * len(df), 3))
    ax = sns.barplot(x='Topology', y='Compression', hue='Type', data=df)

    hatches = ['x', '\\', '*', 'o']

    for i, bar in enumerate(ax.containers):
        plt.bar_label(bar, fmt='%.4f')

    plt.ylabel('Compression', weight='bold', fontsize=12);
    plt.xlabel('Topology', weight='bold', fontsize=12)
    plt.tight_layout(pad=.2)

    plt.ylim([0, .6])

    plt.savefig(args.fout)
