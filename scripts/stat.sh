#!/bin/bash

CNT=0
while :
do
    if ! ps -p $PID > /dev/null
    then
        break
    fi
    PS=$(ps -p $PID -o %cpu,vsz | awk 'NR==2')
    # MEM=$(pmap $PID|tail -n 1|awk '{print $2}')
    TCPU=$(ps -A -o %cpu|tail -n +2|paste -sd+ |bc)
    echo $TCPU $PS
    (($CNT++))
    sleep $1
done
